const express = require("express");
const cors = require("cors");
const mongoose = require("./src/config/database");
const { port } = require("./src/config/config");
const morgan = require("morgan");
const apiConnect = require("./src/routes");
const formData = require("express-form-data");

const bodyParser = express.json();

// database connection
function main() {
  const app = express();
  app.use(cors());
  app.use(bodyParser);
  app.use(formData.parse());

  // connect mongodb
  mongoose;

  app.use(morgan("dev"));

  //   app.use("/api", apiConnect);
  app.use(express.static("public"));
  // Listening to port on defined port
  app.listen(port, () => console.log(`Listening on port: ${port}`));
}
main();
