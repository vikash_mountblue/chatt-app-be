const dotenv = require("dotenv");

// configure the env variable
dotenv.config();
module.exports = {
  port: process.env.PORT,
  jwtSecret: process.env.JWT_SECRET,
};
